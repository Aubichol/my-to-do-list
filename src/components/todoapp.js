import React from 'react';
import {Route, Switch} from 'react-router-dom';
import VisibleTodoList from '../containers/visibletodolist';
import Footer from './footer';
import AddTodo from '../containers/addtodo';

const TodoApp = () => (
        <div>
                <AddTodo />
                <Switch>
                        <Route exact path="/" component={VisibleTodoList}/>
                        <Route path="/:filter" component={VisibleTodoList}/>
                </Switch>
                <Footer />
        </div>
);

export default TodoApp;
//<Route exact path="/" component={VisibleTodoList}/>
