import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Root from './components/root';
import './index.css';
import configureStore from './configurestore';

const store = configureStore();

ReactDOM.render(<Root store={store}/>, document.getElementById('root'));

registerServiceWorker();

//testAddTodos();
//testToggleTodos();

//import {testAddTodos, testToggleTodos} from './reducers/todo';
/*class Provider extends Component {
getChildContext() {
return {
store: this.props.store
};
}
render() {
return this.props.children;
}
}

Provider.childContextTypes = {
store: PropTypes.object
};
*/
