import React from 'react';
import {connect} from 'react-redux';
import {addTodo} from '../actions/actions';

let AddTodo = ({dispatch}) => {
        let input;

        return (
                <div>
                        <input  placeholder='TODO'
                                ref={node => {
                                        input = node;
                                }}
                        />
                        <button onClick={() => {
                                dispatch(addTodo(input.value));
                                input.value='';
                        }}>
                                Add Todo
                        </button>
                </div>
        );
};

//connect function without any argument doesn't subscribe to the store and maps store.dispatch to props as dispatch
export default AddTodo = connect()(AddTodo);
