// import {connect} from 'react-redux';
// import Link from '../components/link';
import React from 'react';
import {Link} from 'react-router-dom';
// import {setVisibilityFilter} from '../actions/actions';

// const mapStateToLinkProps = (state, ownProps) => ({
//         active: ownProps.filter === state.visibilityFilter
// });
//
// const mapDispatchToLinkProps = (dispatch, ownProps) => ({
//         onFilterClick() {
//                 dispatch(setVisibilityFilter(ownProps.filter));
//         },
// });
//
// const FilterLink = connect(mapStateToLinkProps, mapDispatchToLinkProps)(Link);


const FilterLink = ({filter, children}) => (
        <Link
                to={filter === 'all'? '' : filter}
                activestyle={{
                        textDecoration: 'none',
                        color: 'black'
                }}
        >
                {children}
        </Link>
);

export default FilterLink;
