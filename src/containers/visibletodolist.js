import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import TodoList from '../components/todolist';
import * as actions from '../actions/actions';
import {getVisibleTodos, getErrorMessage, getIsFetching} from '../reducers/index';
import FetchError from '../components/fetcherror';

class VisibleTodoList extends Component {
        componentDidMount() {
                this.fetchData();
        }

        componentDidUpdate(prevProps) {
                if(this.props.filter !== prevProps.filter) {
                        this.fetchData();
                }
        }

        fetchData() {
                const {filter, fetchTodos} = this.props;
                fetchTodos(filter).then(() => console.log('done!'));
        }

        render() {
                const {toggleTodo, errorMessage, todos, isFetching} = this.props;
                if(isFetching && !todos.length) {
                        return <p>Loading....</p>;
                }
                if(errorMessage && !todos.length) {
                        return (
                                <FetchError
                                        message={errorMessage}
                                        onRetry={() => this.fetchData()}
                                />
                        )
                }

                return (
                        <TodoList
                                todos={todos}
                                onTodoClick={toggleTodo}
                        />
                );
        }
}

const mapStateToTodoListProps = (state, ownProps) => {
        const filter = ownProps.match.params.filter || 'all';
        return {
                todos: getVisibleTodos(state, filter),
                isFetching: getIsFetching(state, filter),
                errorMessage: getErrorMessage(state, filter),
                filter,
        };
};

// const mapDispatchToTodoListProps = (dispatch) => ({
//         onTodoClick: (id) => {
//                 dispatch(toggleTodo(id));
//         }
// });

// VisibleTodoList = withRouter(connect(mapStateToTodoListProps, {onTodoClick: toggleTodo, receiveTodos})(VisibleTodoList));
VisibleTodoList = withRouter(connect(mapStateToTodoListProps, actions)(VisibleTodoList));

export default VisibleTodoList;
