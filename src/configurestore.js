import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
// import throttle from 'lodash/throttle';
import todos from './reducers/index';
// import {loadState, saveState} from './services/localstorage';

/**
@applyMiddleware is an enhancer
promise and logger ar packages that implement logger and promise functionality
*/


/*middleware is a powerful tool which works within the path of an action creator to store.dispatch,
 helpful for logger, analytics, asynchronous counterflow etc.
*/
// const logger = store => next => {
//         if(!console.group) {
//                 return next;
//         }
//
//         return action => {
//                 console.group(action.type);
//                 console.log('%c prev state: ', 'color: gray', store.getState());
//                 console.log('%c action: ', 'color: blue', action);
//                 const returnValue = next(action);
//                 console.log('%c next state: ', 'color: green', store.getState());
//                 console.groupEnd(action.type);
//                 return returnValue;
//         };
// };

// const promise = store => next => action => {
//         if(typeof action.then === 'function') {
//                 return action.then(next);
//         }
//         return next(action);
// };

// const wrapDispatchWithMiddlewares = (store, middlewares) => {
//         middlewares.slice().reverse().forEach(middleware =>
//                 store.dispatch = middleware(store)(store.dispatch)
//         );
// };

// const thunk = store => next => action =>
//         typeof action === 'function' ? action(store.dispatch, store.getState) : next(action);

const configureStore = () => {
        // const persistedState = loadState();
        // const store = createStore(todoApp, persistedState);

        const middlewares = [thunk];

        if(process.env.NODE_ENV !== 'production') {
                middlewares.push(logger);
                // store.dispatch = addLoggingToDispatch(store);
        }

        // middlewares.push(promise);
        // store.dispatch = addPromiseSupportToDispatch(store);

        // wrapDispatchWithMiddlewares(store, middlewares);

        // const store = createStore(todoApp, applyMiddleware(...middlewares));
        return createStore(todos, applyMiddleware(...middlewares));
        // store.subscribe(throttle(() => {
        //         saveState({
        //                 todos: store.getState().todos
        //         });
        // }, 1000));

        // return store;
};

export default configureStore;
