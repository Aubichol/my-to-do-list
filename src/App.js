import React from 'react';
import TodoApp from './components/todoapp';

const App = () => (<TodoApp />);

export default App;
