import {combineReducers} from 'redux';
import todos, * as todosReducer from './todos';
// import visibilityFilter from './visibilityfilter';

const todoApp = combineReducers({
        todos,
        //visibilityFilter
});

//export default todoApp;

//export const getVisibleTodos = (state, filter) =>
        todosReducer.getVisibleTodos(state.todos, filter);
