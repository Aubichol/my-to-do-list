<<<<<<< HEAD
function counter(state, action) {
        return state;
}

expect(
        counter(0, {type: 'INCREMENT'})
).toEqual(1);
=======
const counter = (state = 0, action) => {
        switch (action.type) {
                case 'INCREMENT':
                        return state + 1;
                case 'DECREMENT':
                        return state - 1;
                default:
                        return state;
        }
}

import {createStore} from 'redux';

const store = createStore(counter);

const Counter = ({value}) => {
        <h1>{value}</h1>
};

const render = () => {
        ReactDOM.render(
                <Counter value={store.getState()}/>,
                document.getElementById("root");
        );
};

store.subscribe(render);

document.addEventListener('click', () => {
        store.dispatch({type: 'INCREMENT'});
});
