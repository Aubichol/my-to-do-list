import {combineReducers} from 'redux';
// import todo from './todo';
import byId, * as fromById from './byid';
import createList, * as fromCreateList from './createlist';

const listByFilter = combineReducers({
        all: createList('all'),
        active: createList('active'),
        completed: createList('completed'),
});

const todos = combineReducers({
        byId,
        listByFilter,
        // allIds,
});

export default todos;

// const getAllTodos = (state) =>
//         state.allIds.map(id => state.byId[id]);

export const getVisibleTodos = (state, filter) => {

        const ids = fromCreateList.getIds(state.listByFilter[filter]);
        return ids.map(id => fromById.getTodo(state.byId, id));


        // const allTodos = getAllTodos(state);
        // switch (filter) {
        //         case 'all':
        //                 return allTodos;
        //         case 'active':
        //                 return allTodos.filter(t => !t.completed);
        //         case 'completed':
        //                 return allTodos.filter(t => t.completed);
        //         default:
        //                 throw new Error(`Invalid filter ${filter}`);
        // }
};


export const getIsFetching = (state, filter) => fromCreateList.getIsFetching(state.listByFilter[filter]);

export const getErrorMessage = (state, filter) => fromCreateList.getErrorMessage(state.listByFilter[filter]);

// AddTodo = connect(
//     null, null
// )(AddTodo);

// AddTodo = connect(
//     state => {
//         return {};
//     },
//     dispatch => {
//         return {dispatch};
//     }
// )(AddTodo);

// AddTodo.contextTypes = {
//         store: PropTypes.object
// };

// class FilterLink extends Component {
//         componentDidMount() {
//                 const {store} = this.context;
//                 this.unsubscribe = store.subscribe(() =>
//                         this.forceUpdate()
//                 );
//         }
//
//         componentWillUnmount() {
//                 this.unsubscribe();
//         }
//
//         render() {
//                 const props = this.props;
//                 const {store} = this.context;
//                 const state = store.getState();
//
//                 return (
//                         <Link   active={
//                                         props.filter===state.visibilityFilter
//                                 }
//                                 onFilterClick={() =>
//                                         store.dispatch({
//                                                 type: 'SET_VISIBILITY_FILTER',
//                                                 filter: props.filter
//                                         })
//                                 }
//                         >
//                                 {props.children}
//                         </Link>
//                 );
//         }
// }
//
// FilterLink.contextTypes = {
//         store: PropTypes.object
// };

/*class VisibleTodoList extends Component {
componentDidMount() {
const {store} = this.context;
this.unsubscribe = store.subscribe(() =>
this.forceUpdate()
);
}

componentWillunmount() {
this.unsubscribe();
}

render() {
const props = this.props;
const {store} = this.context;
const state = store.getState();

return (
<TodoList       todos={
getVisibleTodos(
state.todos,
state.visibilityFilter
)
}
onTodoClick={id =>
store.dispatch({
type: 'TOGGLE_TODO',
id
})
}
/>
);
}
}

VisibleTodoList.contextTypes = {
store: PropTypes.object
};*/

/*
const combineReducers = (reducers) => {
        return (state = {}, action) => {
                return Object.keys(reducers).reduce(
                        (nextState, key) => {
                                nextState[key] = reducers[key](
                                        state[key],
                                        action
                                );
                                return nextState;
                        },
                        {}
                );
        };
};
*/

/*
const todoApp = combineReducers({
        todos: todos,
        visibilityFilter: visibilityFilter
});*/

/*const todoApp = (state = {}, action) => {
        return {
                todos: todos(
                        state.todos,
                        action
                ),
                visibilityFilter: visibilityFilter(
                        state.visibilityFilter,
                        action
                )
        };
};*/

// export const testAddTodos = () => {
//         const stateBefore = [];
//         const action = {
//                 type: 'ADD_TODO',
//                 id: 0,
//                 text: 'Learn Redux'
//         };
//         const stateAfter = [
//                 {
//                         id: 0,
//                         text: 'Learn Redux',
//                         completed: false
//                 }
//         ];
//         deepFreeze(stateBefore);
//         deepFreeze(action);
//         expect(
//                 todos(stateBefore, action)
//         ).toEqual(stateAfter);
//         console.log('All tests passed');
// };
//
// export const testToggleTodos = () => {
//         const stateBefore = [
//                 {
//                         id: 0,
//                         text: 'Learn Redux',
//                         completed: false
//                 },
//                 {
//                         id: 1,
//                         text: 'Make App',
//                         completed: false
//                 }
//         ];
//         const action = {
//                 type: 'TOGGLE_TODO',
//                 id: 1
//         };
//         const stateAfter = [
//                 {
//                         id: 0,
//                         text: 'Learn Redux',
//                         completed: false
//                 },
//                 {
//                         id: 1,
//                         text: 'Make App',
//                         completed: true
//                 }
//         ];
//         deepFreeze(stateBefore);
//         deepFreeze(action);
//         expect(
//                 todos(stateBefore, action)
//         ).toEqual(stateAfter);
//         console.log('All tests passed');
// };


//
// console.log('Initial State:');
// console.log(store.getState());
// console.log('--------------');
//
// console.log('Dispatching ADD_TODO');
// store.dispatch({
//         type: 'ADD_TODO',
//         id: 0,
//         text: 'Learn Redux'
// });
// console.log('Current State:');
// console.log(store.getState());
// console.log('--------------');
//
// console.log('Dispatching ADD_TODO');
// store.dispatch({
//         type: 'ADD_TODO',
//         id: 1,
//         text: 'Make todo'
// });
// console.log('Current State:');
// console.log(store.getState());
// console.log('--------------');
//
// console.log('Dispatching TOGGLE_TODO');
// store.dispatch({
//         type: 'TOGGLE_TODO',
//         id: 1
// });
// console.log('Current State:');
// console.log(store.getState());
// console.log('--------------');
//
// console.log('Dispatching SET_VISIBILITY_FILTER');
// store.dispatch({
//         type: 'SET_VISIBILITY_FILTER',
//         filter: 'SHOW_COMPLETED'
// });
// console.log('Current State:');
// console.log(store.getState());
// console.log('--------------');

// const TodoApp = ({todos}) => {
//         <div>
//                 <input
//                         ref={node => {this.input = node;}}
//                 />
//                 <button onClick={() => {
//                         store.dispatch({
//                                 type: 'ADD_TODO',
//                                 text: 'Test',
//                                 id: nextTodoId++
//                         });
//                 }}>
//                         Add Todo
//                 </button>
//                 <ul>
//                         {todos.map(todo =>
//                                 <li key={todo.id}>
//                                 {todo.text}
//                                 </li>
//                         )}
//                 </ul>
//         </div>
// };
